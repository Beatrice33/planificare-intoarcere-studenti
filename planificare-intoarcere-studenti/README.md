# Student return planning
This project was useful at the beginning of the Covid19 crisis when all the students from Transilvania University of Brasov had to leave the dormitories to continue their studies from home. 
The aim of this application was to avoid the agglomeration and to maintain physical distancing at the end of the semester when the students come back to check out.

![](Images/Capture1.PNG)

## Functionalities
The administrators of the Transilvania University dormitories have access to a list with all the rooms they administer and a filter for the floor. Using the date picker they can choose a time frame in which the students from a certain room can come to check out and to take the things left in the room. The evidence of the scheduled rooms is based on colors: green (scheduled) or red (not scheduled).

This project contains a second part made by one of my colleagues which was designed for the students. Every student from a room could choose only one day from the time frame allocated by the administrators. In this way it couldn't be two students from the same room scheduled in the same day.

## Installation

Install dependencies
```bash
  npm install
```
Run the app 
```bash
  npm start
```








