/**
 *  Metoda care returneaza numele utilizatorului care este logat pe Intranet
 */
export const getUsername = () => {
    if (!process.env.NODE_ENV || process.env.NODE_ENV === "development") {
        //pe aceasta ramura e codul care se executa in mediul de development. E hardcodat sa returneze un mail de profesor, daca lucrati pe pagina de admin, puneti un mail de admin
        return "paul.mirela@unitbv.ro";
    } else {
        //ramura pentru mediul din productie care ia valoarea din tag-ul de html care extrage username-ul din DNN. Tag-ul esta e in index.html
        let element = document.getElementById("userid");
        return element.value;
    }
};

//george.grecu@unitbv.ro
//beatrice.balan@student.unitbv.ro
//valeria.timo@student.unitbv.ro
//maican@unitbv.ro
//ilie.podaru@student.unitbv.ro

//diana.sandu@student.unitbv.ro

//mihaela.popa@student.unitbv.ro