import axios from "axios";

const instance = axios.create({
  baseURL: process.env.REACT_APP_URL_AGSIS,
  headers: {"content-type": "application/json;charset=UTF-8"},
});

export default instance;
