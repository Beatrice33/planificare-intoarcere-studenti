import React, {Component} from 'react'
import axios from "../../APIs";
import localAxios from "../../localAPIs";
import {getUsername} from "../../GetUsername";
import "./AlocareTermenCamera.css"
import Camera from "../../Components/Camera";
import moment from 'moment'
import 'moment/locale/ro';
import {Dropdown,  Grid, Segment, Button,  Menu} from "semantic-ui-react";
import {DateRangePicker} from 'react-dates';
import 'react-dates/lib/css/_datepicker.css';
import 'react-dates/initialize';

class AlocareTermenCamera extends Component {

  state = {
    detaliiCamere: [],
    listaCamine: [],
    caminSelectat: null,
    idCaminSelectat: null,
    listaEtaje: [],
    etajSelectat: [],
    anUniversitarCurent: null,
    anCurentSiPrecedent: [],
    username: null,
    idfacultateCameraNew: null,
    ok: true,
    open: false,
    etajCurent: null,
    etajCamera: null,
    idCamera: '',
    nrLocuri: '',
    checked: true,
    verificat: false,
    idCameraChecked: [],
    dateCamera: []
  }


  componentDidMount() {
    moment.locale('ro');
    const username = getUsername();
    this.setState({username: username});

    axios
      .get("api/AnUniversitar/AnUniversitarCurent")
      .then(anUniv => {
        this.setState({
          anSelectat: anUniv.data.ID_AnUniv
        })
        axios
          .get(
            "api/Camin/CaminListByAnUniv?ID_AnUniv=" +
            anUniv.data.ID_AnUniv
          )
          .then(camine => {
            axios
              .get(
                "api/Camin/CaminListByUsernameAdministrator?UsernameAdministrator=" +
                username
              )
              .then(admin => {
                let lista = [];
                camine.data.forEach((camin) => {
                  admin.data.forEach((adminCamin, index) => {
                    if (camin.ID_Camin === adminCamin.ID_Camin) {
                      let caminNou = {
                        key: index,
                        text: camin.DenumireCamin,
                        value: camin.ID_Camin
                      };
                      lista.push(caminNou);
                    }
                  });

                });
                this.setState({listaCamine: lista});

                if (lista.length >= 0) {
                  this.setState({caminSelectat: lista[0].value, idCaminSelectat: lista[0].value}) //caminSelectat/idCamin=numele/id camin aferent adminului din anul curent
                  localAxios
                    .get(
                      "Camera/CameraListByCaminAnUniv?iD_Camin=" +
                      lista[0].value +
                      "&iD_AnUniv=" +
                      anUniv.data.ID_AnUniv
                    )
                    .then(camere => {
                      this.setState({detaliiCamere: camere.data})
                    })
                  axios
                    .get(
                      "api/Camin/CaminEtajListByCamin?iD_Camin=" +
                      lista[0].value +
                      "&iD_AnUniv=" +
                      anUniv.data.ID_AnUniv
                    )
                    .then(etaje => {
                      let temp = [];
                      etaje.data.forEach((et, index) => {
                        let etaj = {
                          key: index,
                          value: et.Etaj,
                          text: "Etajul " + et.Etaj
                        };
                        temp.push(etaj);
                      });
                      this.setState({listaEtaje: temp});
                    });
                }
              })
          });
      });
  }

  handleCheckbox = (event) => {
    this.state.checked? this.setState({checked: false}) : this.setState({checked: true})
    const value = event.target.value;
    const check = event.target.checked

    if ((check === true) && (this.state.idCameraChecked.includes(value) === false)) {
      let newList = [...this.state.idCameraChecked];
      newList.push(value);
      this.setState({idCameraChecked: newList});
    }

    if ((check === false) && (this.state.idCameraChecked.includes(value) === true)) {
      for (let i = 0; i < this.state.idCameraChecked.length; i++)
        if (this.state.idCameraChecked[i] === value) {
          let newList = [...this.state.idCameraChecked];
          newList.splice(i, 1);
          this.setState({idCameraChecked: newList})
        }
    }
  };

  alegeEtajul = (e, etajSelectat) => {
    this.setState(etajSelectat)
    const etajSelectat1 = etajSelectat.value
    this.setState({etajCurent: etajSelectat1})
  }

  selectieCamin = (e, selectieNoua) => {
    this.setState({detaliiCamere: []});
    this.setState({listaEtaje: []});
    this.setState({etajCurent: null});
    this.setState({caminSelectat: selectieNoua.value});

    this.setState({loaded: false});

    localAxios
      .get(`DateIntoarcereCamin/CameraListByCaminAnUniv?iD_Camin=${selectieNoua.value}&ID_AnUniv=${this.state.anSelectat}`)
      .then(camere => {
        this.setState({detaliiCamere: camere.data})
      })
    axios
      .get(
        "api/Camin/CaminEtajListByCamin?iD_Camin=" +
        selectieNoua.value +
        "&iD_AnUniv=" +
        this.state.anSelectat
      )
      .then(etaje => {
        let temp = [];
        etaje.data.forEach((et, index) => {
          let etaj = {
            key: index,
            value: et.Etaj,
            label: "Etajul " + et.Etaj
          };
          temp.push(etaj);
        });
        this.setState({listaEtaje: temp});
      });

  };

  returneazaDenumireCamin = idCamin => {
    let nume = null;
    this.state.listaCamine.forEach((item) => {
      if (item.value === idCamin) nume = item.text;
    })
    return nume;
  }

  salveazaDate = () => {
    for (let i = 0; i < this.state.idCameraChecked.length; i++) {
      let cameraList = []
      let dataInceput = this.state.startDate.format("YYYY-MM-DD")
      let dataSfarsit = this.state.endDate.format("YYYY-MM-DD")
      for (let id of this.state.idCameraChecked) {
        cameraList.push({
          ID_Camera: id,
          DataInceput: dataInceput,
          DataSfarsit: dataSfarsit
        })
      }

      localAxios
          .post("DateIntoarcereCamin/AdaugareDataIntoarcereCameraCaminList", cameraList)
          .then(() => {
            localAxios
                .get(`DateIntoarcereCamin/CameraListByCaminAnUniv?iD_Camin=${this.state.caminSelectat}&ID_AnUniv=${this.state.anSelectat}`)
                .then(camere => {
                  this.setState({detaliiCamere: camere.data})

                })

            var checkboxes = document.querySelectorAll('input[type=checkbox]')
            for (var j in checkboxes) {
              if (checkboxes[j].checked === true) {
                checkboxes[j].checked = false;
              }
            }
            this.setState({idCameraChecked: []})
          })
          .catch(error => {
            console.log("error" + error)
            alert("a aparut o eroare");
          })
    }
  }




  render() {
    return (
      <div>
        <h1 style={{textAlign: 'center', marginBottom:'30px'}}>
          Pagină Administrator {this.returneazaDenumireCamin(this.state.caminSelectat)}{" "}</h1>
        <Menu widths={3} className="menu2">
          <Menu.Item>
            <Dropdown
              className="dropdownHeader"
              fluid
              selection
              search
              placeholder='Selectați caminul'
              options={this.state.listaCamine}
              onChange={this.selectieCamin}
              value={this.state.caminSelectat}
            />
          </Menu.Item>
          <Menu.Item>
            <Dropdown
              className="dropdownHeader"
              fluid
              selection
              search
              placeholder='Alegeți etajul'
              onChange={this.alegeEtajul}
              options={this.state.listaEtaje}
              value={this.state.etajCurent}/>
          </Menu.Item>
          <Menu.Item><Button type="button" primary>Descarcă Rapoarte</Button></Menu.Item>
        </Menu>
        <div>
          <Segment
            textAlign={"center"}
            style={{marginLeft: 'auto', marginRight: 'auto'}}>
            <div style={{marginLeft: "auto", marginRight: "auto"}}>
              <span>
                <DateRangePicker
                  startDate={this.state.startDate}
                  startDateId="your_unique_start_date_id"
                  endDate={this.state.endDate}
                  endDateId="your_unique_end_date_id"
                  onDatesChange={({startDate, endDate}) => this.setState({
                    startDate,
                    endDate
                  })}
                  focusedInput={this.state.focusedInput}
                  onFocusChange={focusedInput => this.setState({focusedInput})}
                />
              </span>
              <span>
              <Button
                disabled={!this.state.startDate || !this.state.endDate}
                type="button"
                color='green'
                icon='check'
                className="saveButton"
                onClick={this.salveazaDate}
                content='Salvează Modficările'
              />
            </span>
            </div>
            <div>
              <Grid>
                <Grid columns={3}>
                  {(this.state.detaliiCamere
                    .filter(camere => !this.state.etajCurent ? true : camere.Etaj === this.state.etajCurent)
                    .map(show => {
                        return (
                          <Grid.Column width={5} key={show.ID_Camera}>
                            <Camera
                              key={show.ID_Camera}
                              value={show.ID_Camera}
                              name={show.NumarCamera}
                              handleCheckbox={this.handleCheckbox}
                              ID_Camera={show.ID_Camera}
                              NumarCamera={show.NumarCamera}
                              idCamin={this.state.idCamin}
                              verificat={this.state.verificat}
                              dataInceput={show.DataInceputIntoarceri}
                              dataSfarsit={show.DataSfarsitIntoarceri}
                            />
                          </Grid.Column>
                        )
                      }
                    ))}
                </Grid></Grid></div>
          </Segment>
        </div>
      </div>

    )

  }
}

export default AlocareTermenCamera;